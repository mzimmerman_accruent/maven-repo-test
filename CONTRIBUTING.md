Don't create a PR until your feature code is done, dev tested, and unit tested.
If you make a change after creating a PR, do so in a separate commit so others can see what changed.
Before merging PR, squash commits into one to keep develop history clean.